# Logo
* traced from bitmap using inkscape (some manual post processing)
* color palatte for the website pulls from the logo (rock-black, rock-orange)
* closest matching open font (based on c and t characters) Liberation Sans
* the rock-black actually has a bit of grey in it

# Header
* slategrey looks like a perfect match for background color
* added padding to logo to prevent rock-black border from touching logo
* nav extends for larger displays
* nav appears beneath the logo for smaller displays
* nav links are shaded in when hovered over
* an h1 header containing "Rock Coast" is hidden from view for screenreaders
* logo alt text is kept short to avoid wasting screenreader users time
* logo has dimensions inlined so browsers will scale it accordingly

# Main
* h2's and card backgrounds use rock-orange
* comprised of sections (commonmark compatible)
* css only rounds the first and last section
  (additional sections may be inserted *duplicate*)
* the products section has special styling to display cards in a grid
* rows and columns are dynamicaly determined
* h2's correspond to sections for screenreaders

# Cards
* cards must be comprised of three elements
  (summary, description list, and buy now)
* summaries are blocks and may use commonmark
* summaries are attached to top to fill space
* all corners rounded, even on horizontal rules
  (there is a Windows 11 joke to be made somehow)
* description lists are assumed to be of similar length
  (cards are wider to accommodate longer definitions)
* buy now is attache to bottom to fill space
* buy now link will darken on hover to match nav link behaviour
* overflow in summary or description list is gracefully handled
* card length can be adjusted to prevent overflow from breaking uniformity
  (style.css :root --card-min-height)
* cards shrink to fit a mobile format

#CSS
* mostly follows document order
* implicit sections (but could use some comments)
