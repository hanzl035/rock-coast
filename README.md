The *public* directory contains all the files for running the website. Files
outside the directory were used to generate assets.

There are two variables in *style.css* that can be used to adjust the size
of the cards (near the top in the *:root* section).

* **--card-min-height:** adjusts the height of a card (default 33em) 
* **--card-max-width:** adjusts the width of a card (default 22em)
